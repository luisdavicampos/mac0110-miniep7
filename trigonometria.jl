# MAC0110 - MiniEP7
# Luís Davi Oliveira de Almeida Campos - 11849460

# Parte 1

function sin(x)
    atual = x
    for i in 1 : 10
        if i % 2 == 1
            atual = atual - (x^(2i + 1))/factorial(big(2i + 1))
        else
            atual = atual + (x^(2i + 1))/factorial(big(2i + 1))
        end
    end
    return atual
end

function cos(x)
    atual = 1
    for i in 1 : 10
        if i % 2 == 1
            atual = atual - (x^(2i))/big(factorial(2i))
        else
            atual = atual + (x^(2i))/big(factorial(2i))
        end
    end
    return atual
end

function tan(x)
    atual = 0
    for n in 1 : 10
        atual = atual + (2^(2n) * (2^(2n) - 1) * bernoulli(n) * x^(2n-1))/big(factorial(2n))
    end
    return BigFloat(atual)
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

# Parte 2

function approx(a, b)
    if abs(a - b) < 0.001
        return true
    else
        return false
    end
end

function check_sin(value, x)
    approx(value, sin(x))
end

function check_cos(value, x)
    approx(value, cos(x))
end

function check_tan(value, x)
    approx(value, tan(x))
end

# Parte 3

function taylor_sin(x)
    atual = x
    for i in 1 : 10
        if i % 2 == 1
            atual = atual - (x^(2i + 1))/factorial(big(2i + 1))
        else
            atual = atual + (x^(2i + 1))/factorial(big(2i + 1))
        end
    end
    return atual
end

function taylor_cos(x)
    atual = 1
    for i in 1 : 10
        if i % 2 == 1
            atual = atual - (x^(2i))/big(factorial(2i))
        else
            atual = atual + (x^(2i))/big(factorial(2i))
        end
    end
    return atual
end

function taylor_tan(x)
    atual = 0
    for n in 1 : 10
        atual = atual + (2^(2n) * (2^(2n) - 1) * bernoulli(n) * x^(2n-1))/big(factorial(2n))
    end
    return BigFloat(atual)
end

using Test
function test()
    #tests sin
    @test approx(sin(π/2), 1.0) == true
    @test approx(sin(π/6), 0.5) == true
    @test approx(sin(0), 0.0) == true
    #tests cos
    @test approx(cos(π/2), 0.0) == true
    @test approx(cos(π/6), sqrt(3)/2) == true
    @test approx(cos(π), -1.0) == true
    #tests tan
    @test approx(tan(0), 0.0) == true
    @test approx(tan(π/4), 1.0) == true
    @test approx(tan(π/3), sqrt(3)) == true
    #tests check_sin
    @test check_sin(1.0, π/2) == true
    @test check_sin(0.0, 0) == true
    @test check_sin(sqrt(2)/2, π/4) == true
    #tests check_cos
    @test check_cos(0.0, π/2) == true
    @test check_cos(1.0, 0) == true
    @test check_cos(-sqrt(2)/2, 3π/4) == true
    #tests check_tan
    @test check_tan(0.0, 0) == true
    @test check_tan(sqrt(3)/3, π/6) == true
    @test check_tan(1, π/4) == true
end